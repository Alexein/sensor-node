#include "loraio.h"

radio_layer_msg_header hdr;
radio_layer_msg_header rcvhdr;
radio_msg_sensor_frame rcvframe;

void radio_set(uint8_t rcv_id, uint8_t snd_id){
    hdr.receiver_id = rcv_id;
    hdr.sender_id = snd_id;
}

void radio_send(lora dev, radio_msg_sensor_frame msgf){
    hdr.msg_id++;
    hdr.payload_len = sizeof(radio_msg_sensor_frame);
    lora_begin_packet(&dev, false);
    lora_write_data(&dev, (const uint8_t*)&hdr, sizeof(hdr));
    lora_write_data(&dev, (const uint8_t*)&msgf, sizeof(msgf));
    lora_end_packet(&dev, false);

    lora_receive(&dev, 0);
}

/*static void on_rx_done(void *ctx, int packet_size){
    lora *const dev = (lora *const) ctx;
    
    uint8_t *p = (uint8_t*)&rcvframe;
    size_t i = 0;

    if(packet_size == 0)    goto err;

    rcvhdr.receiver_id = lora_read(dev);
    rcvhdr.sender_id = lora_read(dev);
    rcvhdr.msg_id = lora_read(dev);
    rcvhdr.payload_len = lora_read(dev);

    if((rcvhdr.receiver_id != hdr.sender_id) | (rcvhdr.payload_len != sizeof(radio_msg_sensor_frame)))      goto err;

    while((lora_available(dev)) & (i < rcvhdr.payload_len)){
        *(p+i) = (uint8_t)lora_read(dev);
        i++;
    }

    err:    lora_receive(dev, 0);
}*/