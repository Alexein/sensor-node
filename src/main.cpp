#include "mbed.h"
#include "DHT.h"
#include "lora-mbed.h"
#include "loraio.h"
#include <cstdint>

DHT sensor(PB_10, DHT22);

const uint8_t receiver_id = 0x01;               //router
const uint8_t sender_id = 0x10;                 //this sensor

SPI spi(PB_5, PB_4, PB_3);
DigitalOut nss(PB_10);
DigitalInOut rst(PA_8);
InterruptIn dio0(PA_9);

lora dev;
radio_msg_sensor_frame msg;

Ticker t;
InterruptIn alarm(PA_8, PullUp);
bool data_ready = false;

void send_alarm(){
    msg.alarmSet = 1;
    //radio_send(dev, msg);
    //msg.alarmSet = 0;
}

void send_data(){
    msg.temp = sensor.ReadTemperature(CELCIUS);
    msg.hum = sensor.ReadHumidity();
    //radio_send(dev, msg);
}

int main(){
    printf("on");   
    dev.frequency = 433E6;
    dev.on_receive = NULL;
    dev.on_tx_done = NULL;
    radio_set(receiver_id, sender_id);
    
    lora_mbed mbed_dev;
    mbed_dev.spi =   &spi;
    mbed_dev.nss =   &nss;
    mbed_dev.reset = &rst;
    mbed_dev.dio0 =  &dio0;

    lora_mbed_init(&dev, &mbed_dev);

    t.attach(callback(&send_data), 1s);
    alarm.rise(callback(&send_alarm));

    while(1){
        if(data_ready){
            radio_send(dev, msg);
            data_ready = false;
            if(msg.alarmSet == 1) msg.alarmSet = 0;
        }
        thread_sleep_for(500);
    }
}